from openmm.app import *
from openmm import *
from openmm.unit import *

import os

import numpy as np

gro = app.GromacsGroFile("us.gro")
top = app.GromacsTopFile("../../topol.top", 
                         periodicBoxVectors=gro.getPeriodicBoxVectors())

# Get CA positions of chain 1
ca1_id = []
for a in list(top.topology.atoms()):
    if a.residue.chain.id == "1" and a.name == "CA":
        ca1_id.append(a.id)
positionsCA1 = list([gro.getPositions()[int(x)-1] for x in ca1_id])
positionsCA1znm = (
    list(
        [
            gro.getPositions(
                asNumpy=True
            )[int(x)-1][2].value_in_unit(nanometer) 
            for x in ca1_id
        ]
    )
)

# Get CA positions of chain 2
ca2_id = []
for a in list(top.topology.atoms()):
    if a.residue.chain.id == "2" and a.name == "CA":
        ca2_id.append(a.id)
positionsCA2 = list([gro.getPositions()[int(x)-1][:2] for x in ca2_id])
positionsCA2znm = (
    list(
        [
            gro.getPositions(
                asNumpy=True
            )[int(x)-1][2].value_in_unit(nanometer) 
            for x in ca2_id
        ]
    )
)

z0 = np.mean(np.array(positionsCA1znm)) - np.mean(np.array(positionsCA2znm))

system = top.createSystem(
    nonbondedMethod=PME, 
    nonbondedCutoff=1.2*nanometer,
    constraints=HBonds,
)

# Add restraints to chain 1
force1 = CustomExternalForce("k*((x-x0)^2+(y-y0)^2+(z-z0)^2)/2")
force1.addGlobalParameter("k", 1000.0*kilojoule_per_mole/nanometer**2)
force1.addPerParticleParameter("x0")
force1.addPerParticleParameter("y0")
force1.addPerParticleParameter("z0")

system.addForce(force1)
for i,p in zip(ca1_id, positionsCA1):
    force1.addParticle(int(i), p)

# Add restraints to chain 2
# Particle 1
force2 = CustomExternalForce(
    "k*((x-x0)^2+(y-y0)^2)/2+k1*((z0-z/3-z23/3-d0)^2)/2"
)
force2.addGlobalParameter("k", 1000.0*kilojoule_per_mole/nanometer**2)
force2.addGlobalParameter("k1", 10000.0*kilojoule_per_mole/nanometer**2)
force2.addGlobalParameter(
    "z23",
    (
        np.array(positionsCA2znm)[1]
        + np.array(positionsCA2znm)[2]
    ) * nanometer
)
force2.addGlobalParameter("z0", np.mean(np.array(positionsCA1znm))*nanometer)
force2.addGlobalParameter("d0", z0*nanometer)
force2.addPerParticleParameter("x0")
force2.addPerParticleParameter("y0")

# Particle 1
system.addForce(force2)
for i,p in zip(ca2_id[0:1], positionsCA2[0:1]):
    force2.addParticle(int(i), p)

# Particle 2
force3 = CustomExternalForce(
    "k*((x-x0)^2+(y-y0)^2)/2+k1*((z0-z/3-z13/3-d0)^2)/2"
)
force3.addGlobalParameter("k", 1000.0*kilojoule_per_mole/nanometer**2)
force3.addGlobalParameter("k1", 10000.0*kilojoule_per_mole/nanometer**2)
force3.addGlobalParameter(
    "z13",
    (
        np.array(positionsCA2znm)[0]
        + np.array(positionsCA2znm)[2]
    ) * nanometer
)
force3.addGlobalParameter("z0", np.mean(np.array(positionsCA1znm))*nanometer)
force3.addGlobalParameter("d0", z0*nanometer)
force3.addPerParticleParameter("x0")
force3.addPerParticleParameter("y0")

# Particle 2
system.addForce(force3)
for i,p in zip(ca2_id[1:2], positionsCA2[1:2]):
    force3.addParticle(int(i), p)

# Particle 3
force4 = CustomExternalForce(
    "k*((x-x0)^2+(y-y0)^2)/2+k1*((z0-z/3-z12/3-d0)^2)/2"
)
force4.addGlobalParameter("k", 1000.0*kilojoule_per_mole/nanometer**2)
force4.addGlobalParameter("k1", 10000.0*kilojoule_per_mole/nanometer**2)
force4.addGlobalParameter(
    "z12",
    (
        np.array(positionsCA2znm)[0]
        + np.array(positionsCA2znm)[1]
    ) * nanometer
)
force4.addGlobalParameter("z0", np.mean(np.array(positionsCA1znm))*nanometer)
force4.addGlobalParameter("d0", z0*nanometer)
force4.addPerParticleParameter("x0")
force4.addPerParticleParameter("y0")

# Particle 3
system.addForce(force4)
for i,p in zip(ca2_id[2:3], positionsCA2[2:3]):
    force4.addParticle(int(i), p)

# Run pullings
integrator = LangevinIntegrator(
    300*kelvin,
    1/picosecond,
    0.002*picoseconds,
)
simulation = Simulation(top.topology, system, integrator)
simulation.context.setPositions(gro.positions)
simulation.reporters.append(
    StateDataReporter(
        f"log.dat",
        5000,
        step=True,
        potentialEnergy=True,
        temperature=True,
    ),
)

class DistanceReporter(object):
    def __init__(self, file, reportInterval):
        self._out = open(file, 'w')
        self._reportInterval = reportInterval

    def __del__(self):
        self._out.close()

    def describeNextReport(self, simulation):
        steps = self._reportInterval - simulation.currentStep%self._reportInterval
        return (steps, True, False, False, False, True)

    def report(self, simulation, state):
        positionsCA1 = list([state.getPositions(asNumpy=True)[int(x)-1][2].value_in_unit(nanometer) for x in ca1_id])
        positionsCA2 = list([state.getPositions(asNumpy=True)[int(x)-1][2].value_in_unit(nanometer) for x in ca2_id])
        dist = np.mean(np.array(positionsCA1)) - np.mean(np.array(positionsCA2))
        self._out.write('%g\n' % dist)

simulation.reporters.append(DCDReporter(f'traj.dcd', 1000))
simulation.reporters.append(DistanceReporter('dist.txt', 100))

for i in range(5000000):
    state = simulation.context.getState(getPositions=True, enforcePeriodicBox=True)
    positionsCA1 = list([state.getPositions(asNumpy=True)[int(x)-1][2].value_in_unit(nanometer) for x in ca1_id])
    positionsCA2 = list([state.getPositions(asNumpy=True)[int(x)-1][2].value_in_unit(nanometer) for x in ca2_id])

    dist = np.mean(np.array(positionsCA1)) - np.mean(np.array(positionsCA2))
    simulation.context.setParameter(
        "z0", np.mean(np.array(positionsCA1)) * nanometer
    )
    simulation.context.setParameter(
        "z23",
        (
            np.array(positionsCA2)[1]
            + np.array(positionsCA2)[2]
        ) * nanometer
    )
    simulation.context.setParameter(
        "z13",
        (
            np.array(positionsCA2)[0]
            + np.array(positionsCA2)[2]
        ) * nanometer
    )
    simulation.context.setParameter(
        "z12",
        (
            np.array(positionsCA2)[0]
            + np.array(positionsCA2)[1]
        ) * nanometer
    )
    #print(simulation.context.getParameter("d0"), simulation.context.getParameter("z0"), simulation.context.getParameter("z1")/2)
    simulation.step(1)



