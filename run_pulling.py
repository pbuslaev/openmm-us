from openmm.app import *
from openmm import *
from openmm.unit import *

import os

gro = app.GromacsGroFile("new.gro")
top = app.GromacsTopFile("topol.top", 
                         periodicBoxVectors=gro.getPeriodicBoxVectors())

# Get CA positions of chain 1
ca1_id = []
for a in list(top.topology.atoms()):
    if a.residue.chain.id == "1" and a.name == "CA":
        ca1_id.append(a.id)
positionsCA1 = list([gro.getPositions()[int(x)-1] for x in ca1_id])

# Get CA positions of chain 2
ca2_id = []
for a in list(top.topology.atoms()):
    if a.residue.chain.id == "2" and a.name == "CA":
        ca2_id.append(a.id)
positionsCA2 = list([gro.getPositions()[int(x)-1] for x in ca2_id])

system = top.createSystem(
    nonbondedMethod=PME, 
    nonbondedCutoff=1.2*nanometer,
    constraints=HBonds,
)

# Add restraints to chain 1
force1 = CustomExternalForce("k*((x-x0)^2+(y-y0)^2+(z-z0)^2)")
force1.addGlobalParameter("k", 1000.0*kilojoule_per_mole/nanometer**2)
force1.addPerParticleParameter("x0")
force1.addPerParticleParameter("y0")
force1.addPerParticleParameter("z0")

system.addForce(force1)
for i,p in zip(ca1_id, positionsCA1):
    force1.addParticle(int(i), p)

# Add restraints to chain 2
force2 = CustomExternalForce("k*((x-x0)^2+(y-y0)^2+(z-z0+v*t)^2)")
force2.addGlobalParameter("k", 1000.0*kilojoule_per_mole/nanometer**2)
force2.addGlobalParameter("v", 0.000033*nanometer/picosecond) # 0.000033
force2.addGlobalParameter("t", 0*picosecond)
force2.addPerParticleParameter("x0")
force2.addPerParticleParameter("y0")
force2.addPerParticleParameter("z0")

system.addForce(force2)
for i,p in zip(ca2_id, positionsCA2):
    force2.addParticle(int(i), p)

# Run pullings
for i in range(1,6):
    print(i)
    os.mkdir(f"pull_{i}")
    integrator = LangevinIntegrator(
        300*kelvin,
        1/picosecond,
        0.002*picoseconds,
    )
    simulation = Simulation(top.topology, system, integrator)
    simulation.context.setPositions(gro.positions)
    simulation.reporters.append(
        StateDataReporter(
            f"pull_{i}/log.dat",
            500,
            step=True,
            potentialEnergy=True,
            temperature=True,
        ),
    )
    simulation.reporters.append(DCDReporter(f'pull_{i}/traj.dcd', 500))
    for i in range(100000):
        simulation.context.setParameter("t", i*0.002*500*picosecond)
        simulation.step(500)

