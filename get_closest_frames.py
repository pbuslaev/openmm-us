import MDAnalysis as mda
import numpy as np
import os

input_structure = "../NPT_oriented.gro"
input_trajectory = "pull.xtc"


chainAselection = "bynum 1:1371 and name CA"
chainBselection = "bynum 1372:1431 and name CA"

# create universe
u = mda.Universe(input_structure, input_trajectory)

chainA = u.select_atoms(chainAselection)
chainB = u.select_atoms(chainBselection)

distances = np.arange(15.5, 25.5, 0.5)
closest_frames = np.zeros_like(distances, dtype = int)
current_distances = np.full_like(distances, 1000)

for ts in u.trajectory:
	# measure distance
	dist = chainA.center_of_mass()[2] - chainB.center_of_mass()[2]
	closest_frames[
		np.where(np.abs(dist-distances) < np.abs(dist - current_distances))
	] = ts.frame
	current_distances[
		np.where(np.abs(dist-distances) < np.abs(dist - current_distances))
	] = dist

print(closest_frames, current_distances)
for i, closest_frame in enumerate(closest_frames):
	os.mkdir(f"us_{i+1}")
	for ts in u.trajectory[closest_frame:closest_frame+1]:
		u.select_atoms("all").write(f"us_{i+1}/us.gro")



